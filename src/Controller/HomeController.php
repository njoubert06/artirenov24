<?php

namespace App\Controller;

use App\Entity\Chantier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(EntityManagerInterface $em): Response
    {

        $chantiers = $em->getRepository(Chantier::class)->findAll();

        return $this->render('home/index.html.twig', [
            'chantiers' => $chantiers
        ]);
    }
}
