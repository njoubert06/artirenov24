<?php

namespace App\Controller;

use App\Entity\Chantier;
use App\Entity\ImageChantier;
use App\Form\ChantierType;
use App\Repository\ChantierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chantier")
 */
class ChantierController extends AbstractController
{
    /**
     * @Route("/", name="app_chantier_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $chantiers = $entityManager
            ->getRepository(Chantier::class)
            ->findAll();

        return $this->render('chantier/index.html.twig', [
            'chantiers' => $chantiers,
        ]);
    }

    /**
     * @Route("/new", name="app_chantier_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $em): Response
    {
        $chantier = new Chantier();
        $form = $this->createForm(ChantierType::class, $chantier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On récupère les images transmises
            $image = $form->get('images')->getData();

            // On boucle sur les images
            // On génère un nouveau nom de fichier
            $fichier = md5(uniqid()) . '.' . $image->guessExtension();

            // On copie le fichier dans le dossier uploads
            $image->move(
                $this->getParameter('images_directory'),
                $fichier
            );

            // On stocke l'image dans la base de donnée (son nom)
//                $img = new ImageChantier();
            $chantier->setBeforeIllustration($fichier);
//                $chantier->addImageChantier($img);

            $mainImg = $form->get('mainIllustration')->getData();
            $fichierMainImg = md5(uniqid()) . '.' . $mainImg->guessExtension();

            // On copie le fichier dans le dossier uploads
            $mainImg->move(
                $this->getParameter('images_directory'),
                $fichierMainImg
            );

            // On stocke l'image dans la base de donnée (son nom)
            $chantier->setMainIllustration($fichierMainImg);


            $em->persist($chantier);
            $em->flush();

            return $this->redirectToRoute('app_chantier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('chantier/new.html.twig', [
            'chantier' => $chantier,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_chantier_show", methods={"GET"})
     */
    public function show(Chantier $chantier): Response
    {
        return $this->render('chantier/show.html.twig', [
            'chantier' => $chantier,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_chantier_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Chantier $chantier, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ChantierType::class, $chantier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On récupère les images transmises
            $image = $form->get('images')->getData();

            if ($image != []) {

                // On boucle sur les images
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();

                // On copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );

                // On stocke l'image dans la base de donnée (son nom)
                $img = new ImageChantier();
                $img->setName($fichier);
                $chantier->addImageChantier($img);
            }

            $mainImg = $form->get('mainIllustration')->getData();

            if ($mainImg != null) {

                $fichierMainImg = md5(uniqid()) . '.' . $mainImg->guessExtension();

                // On copie le fichier dans le dossier uploads
                $mainImg->move(
                    $this->getParameter('images_directory'),
                    $fichierMainImg
                );

                // On stocke l'image dans la base de donnée (son nom)
                $chantier->setMainIllustration($fichierMainImg);
            }


            $entityManager->flush();

            return $this->redirectToRoute('app_chantier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('chantier/edit.html.twig', [
            'chantier' => $chantier,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_chantier_delete", methods={"POST"})
     */
    public function delete(Request $request, Chantier $chantier, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $chantier->getId(), $request->request->get('_token'))) {
            $entityManager->remove($chantier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_chantier_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @param EntityManagerInterface $em
     * @route("/supprime/image/{id}", name="chantier_delete_image", methods={"DELETE"})
     */
    public function deleteImage(EntityManagerInterface $em, ImageChantier $image, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        //On verifie si le token est valide
        if ($this->isCsrfTokenValid('delete' . $image->getId(), $data['_token'])) {
            //On récupère le nom le l'image
            $nom = $image->getName();
            //On supprime le fichier
            unlink($this->getParameter('images_directory') . '/' . $nom);

            // On supprime l'image de la base de données
            $em->remove($image);
            $em->flush();

            // On répond en json
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token invalide'], 400);
        }
    }
}
