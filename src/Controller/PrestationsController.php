<?php

namespace App\Controller;

use App\Entity\Chantier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PrestationsController extends AbstractController
{
    /**
     * @Route("/prestations", name="prestations")
     */
    public function index(EntityManagerInterface $em): Response
    {

        $chantiers = $em->getRepository(Chantier::class)->findAll();

        return $this->render('prestations/index.html.twig', [
            'chantiers' => $chantiers
        ]);
    }
}
