<?php

namespace App\Controller;

use App\Entity\RequestInformation;
use App\Form\ContactType;
use App\Services\Mail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, EntityManagerInterface $em): Response
    {

        $requestInfo = new RequestInformation();

        $form = $this->createForm(ContactType::class, $requestInfo);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $image1 = $form->get('photo1')->getData();
            $image2 = $form->get('photo2')->getData();
            $image3 = $form->get('photo3')->getData();
            if($image1) {
                $fichier1 = md5(uniqid()) . '.' . $image1->guessExtension();
                $image1->move(
                    $this->getParameter('images_directory'),
                    $fichier1
                );
                $requestInfo->setImageOne($fichier1);
            }
            if($image2) {
                $fichier2 = md5(uniqid()) . '.' . $image2->guessExtension();
                $image2->move(
                    $this->getParameter('images_directory'),
                    $fichier2
                );
                $requestInfo->setImageTwo($fichier2);
            }
            if($image3) {
                $fichier3 = md5(uniqid()) . '.' . $image3->guessExtension();
                $image3->move(
                    $this->getParameter('images_directory'),
                    $fichier3
                );
                $requestInfo->setImageThree($fichier3);
            }

            $em->persist($requestInfo);
            $em->flush($requestInfo);


            $mail = new Mail();
            $mail->send('johan.joubert@adaptiv-prevention.com', 'Johan', 'message', 'test');

            return $this->redirectToRoute('contact');
        }

        return $this->render('contact/index.html.twig', [
            'form'  => $form->createView()
        ]);
    }
}
