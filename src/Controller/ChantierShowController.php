<?php

namespace App\Controller;

use App\Entity\Chantier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ChantierShowController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/nos-chantiers", name="all_chantier")
     */
    public function index(): Response
    {

        $chantiers = $this->em->getRepository(Chantier::class)->findAll();

        return $this->render('chantier_show/index.html.twig', [
            'chantiers' => $chantiers
        ]);
    }

    /**
     * @Route("/nos-chantiers/{id}", name="show_chantier")
     */
    public function show($id): Response
    {
        $allChantiers = $this->em->getRepository(Chantier::class)->findAll();

        $chantier = $this->em->getRepository(Chantier::class)->findOneById($id);

        $previous = null;
        $next = null;

        for($i = 0; $i < count($allChantiers); $i++) {
            if ($allChantiers[$i] == $chantier) {
                if(isset($allChantiers[$i-1])){
                    $previous = $allChantiers[$i-1];
                }
                if(isset($allChantiers[$i+1])){
                    $next = $allChantiers[$i+1];
                }
            }
        }


            return $this->render('chantier_show/show.html.twig', [
                'chantier' => $chantier,
                'previous'  => $previous,
                'next'      => $next
        ]);
    }
}
