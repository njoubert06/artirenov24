<?php

namespace App\Form;

use App\Entity\Chantier;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\TextEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChantierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label'  => 'Nom du chantier',
                'attr'   => [
                    'class' => 'form-control'
                ]
            ])
            ->add('type', TextType::class, [
                'label'  => 'Type (exemple: maison, salle de bain, façade, agrandissement...)',
                'attr'   => [
                    'class' => 'form-control'
                ]
            ])
            ->add('surface', NumberType::class, [
                'label'  => 'Surface en m2',
                'attr'   => [
                    'class' => 'form-control'
                ]
            ])
            ->add('localisation', TextType::class, [
                'label'  => 'Localisation du chantier',
                'attr'   => [
                    'class' => 'form-control'
                ]
            ])

            ->add('description', TextEditorType::class, [
                'label' => 'Description du chantier',
                'attr'  => [
                    'class' => 'summernote'
                ]
            ])
            ->add('mainIllustration', FileType::class, [
                'label'     => "Illustration principale du chantier",
                'required'  => false,
                'mapped'    => false,
                'attr'  => [
                    'class' => 'form-control'
                ]

            ])
            ->add('images', FileType::class, [
                'label'     => "Ajoutez d'autre photos",
                'mapped'    => false,
                'required'  => false,
                'attr'  => [
                    'class' => 'form-control'
                ]

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Chantier::class,
        ]);
    }
}
