<?php

namespace App\Form;

use App\Entity\Chantier;
use App\Entity\RequestInformation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $services = ['Travaux peinture' => "Travaux", 'Sol' => "Sol", 'Façade' => 'Façade'];

        $builder
            ->add('firstName', TextType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder'   => 'Prénom',
                    'class'         => 'form-control'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder'   => 'Nom',
                    'class'         => 'form-control'
                ]
            ])
            ->add('phone', TelType::class, [
                'label' => false,
                'required'  =>false,
                'attr'  => [
                    'placeholder'   => 'Téléphone',
                    'class'         => 'form-control'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder'   => 'Votre e-mail',
                    'class'         => 'form-control'
                ]
            ])

            ->add('locality', TextType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder'   => 'Votre localité',
                    'class'         => 'form-control'
                ]
            ])
//            ->add('services1', CheckboxType::class, [
//                'label'   => 'Travaux peinture sur murs',
//                'required'  => false,
//                'attr'      => [
//                    'value' => 'Peinture'
//                ]
//                ],
//            )
//            ->add('services2', CheckboxType::class, [
//                'label'   => 'Travaux peinture sur plafonds',
//                'value'     => 'peinture',
//
//                'required'  => false,
//                ],
//            )
//            ->add('services3', CheckboxType::class, [
//                'label'   => 'Travaux peinture sur boiseries',
//                'required'  => false,
//                ],
//            )
//            ->add('services4', CheckboxType::class, [
//                'label'   => 'Travaux peinture sur métaux',
//                'required'  => false,
//                ],
//            )
//            ->add('services5', CheckboxType::class, [
//                'label'   => 'Pose de papier peint',
//                'required'  => false,
//                ],
//            )
//            ->add('services6', CheckboxType::class, [
//                'label'   => 'Pose de moquette',
//                'required'  => false,
//                ],
//            )
//            ->add('services7', CheckboxType::class, [
//                'label'   => 'Pose de parquet flottant',
//                'required'  => false,
//                ],
//            )
//            ->add('services8', CheckboxType::class, [
//                'label'   => 'Ponçage et / ou vitrification de parquet',
//                'required'  => false,
//                ],
//            )
//            ->add('services9', CheckboxType::class, [
//                'label'   => 'Ravalement de façade',
//                'required'  => false,
//                ],
//            )
//            ->add('services10', CheckboxType::class, [
//                'label'   => 'Autre',
//                'required'  => false,
//                ],
//            )
            ->add('area', TextType::class, [
                'label' => false,
                'attr'  => [
                    'placeholder'   => 'Surface à traiter (en m2)',
                    'class'         => 'form-control'
                ]
            ])
            ->add('photo1', FileType::class, [
                'label' => 'Photo 1',
                'required'  =>false,
                'mapped'    => false,
                'attr'  => [
                    'class' => 'form-control-file'
                ]
            ])
            ->add('photo2', FileType::class, [
                'label' => 'Photo 2',
                'required'  =>false,
                'mapped'    => false,
                'attr'  => [
                    'class' => 'form-control-file'
                ]
            ])
            ->add('photo3', FileType::class, [
                'label' => 'Photo 3',
                'required'  =>false,
                'mapped'    => false,
                'attr'  => [
                    'class' => 'form-control-file'
                ]
            ])
            ->add('message', TextareaType::class, [
                'label' => false,
                'attr'  => [
                    'class' => 'form-control',
                    'placeholder' => 'Votre message'
                ]
            ])
//            ->add('submit', SubmitType::class, [
//                'label' => 'Envoyer',
//                'attr'  => [
//                    'class' => 'btn btn-yellow'
//                ]
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RequestInformation::class,
        ]);
    }
}
