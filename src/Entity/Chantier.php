<?php

namespace App\Entity;

use App\Repository\ChantierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChantierRepository::class)
 */
class Chantier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=ImageChantier::class, mappedBy="chantier", orphanRemoval=true, cascade={"persist"})
     */
    private $imageChantiers;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainIllustration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $surface;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $localisation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $beforeIllustration;

    public function __construct()
    {
        $this->imageChantiers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, ImageChantier>
     */
    public function getImageChantiers(): Collection
    {
        return $this->imageChantiers;
    }

    public function addImageChantier(ImageChantier $imageChantier): self
    {
        if (!$this->imageChantiers->contains($imageChantier)) {
            $this->imageChantiers[] = $imageChantier;
            $imageChantier->setChantier($this);
        }

        return $this;
    }

    public function removeImageChantier(ImageChantier $imageChantier): self
    {
        if ($this->imageChantiers->removeElement($imageChantier)) {
            // set the owning side to null (unless already changed)
            if ($imageChantier->getChantier() === $this) {
                $imageChantier->setChantier(null);
            }
        }

        return $this;
    }

    public function getMainIllustration(): ?string
    {
        return $this->mainIllustration;
    }

    public function setMainIllustration(string $mainIllustration): self
    {
        $this->mainIllustration = $mainIllustration;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSurface(): ?int
    {
        return $this->surface;
    }

    public function setSurface(int $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getBeforeIllustration(): ?string
    {
        return $this->beforeIllustration;
    }

    public function setBeforeIllustration(?string $beforeIllustration): self
    {
        $this->beforeIllustration = $beforeIllustration;

        return $this;
    }
}
