<?php

namespace App\Services;

use Mailjet\Client;
use Mailjet\Resources;

class Mail {
    private string $key_public = "42d6bad01d060d140adbd743418b1349";
    private string $key_secret = "7fc996203af3ab434ace9edf42088095";

    public function send($to_email, $to_name, $subject, $content) {

        $mj = new Client($this->key_public, $this->key_secret, true,['version' => 'v3.1']);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => "johan.joubert@2jcrea.fr",
                        'Name' => "johan"
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,
                            'Name' => $to_name
                        ]
                    ],
                    'TemplateID'    => 3955918,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content,
                    ]
                ]
            ]
        ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);

        $response->success();

    }

}